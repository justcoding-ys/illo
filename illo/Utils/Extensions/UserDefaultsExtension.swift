import Foundation

protocol KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String
}

extension KeyNamespaceable {
    
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

protocol StringDefaultSettable: KeyNamespaceable {
    associatedtype StringKey: RawRepresentable
}

extension StringDefaultSettable where StringKey.RawValue == String {
    func set(searchHistories: [SearchHistory], forKey key: StringKey) {
        let data = searchHistories.map { $0.property }
        UserDefaults.standard.set(data, forKey: namespaced(key))
    }
    
    @discardableResult
    func object(forKey key: StringKey) -> [SearchHistory]? {
        
        if let propertyPlaceInfos = UserDefaults.standard.object(forKey: namespaced(key)) as? [[String:String]] {
            let result = propertyPlaceInfos.flatMap { SearchHistory(dictionary: $0)}
            return result
        }

        return nil
    }
    
    func removeObject(forKey key: StringKey) {
        UserDefaults.standard.removeObject(forKey: namespaced(key))
    }
}

extension UserDefaults: StringDefaultSettable {
    enum StringKey: String {
        case searchHistory
    }
}
