import UIKit

extension UIView {
    
    static func fadeIn(component: UIView, duration: Float) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {() in
            component.alpha = 1
        })
    }
    
    static func fadeIn(component: UIView, duration: Float, completion: @escaping (Bool) -> ()) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {() in
            component.alpha = 1
        }, completion: { (success) in
            completion(true)
        })
    }
    
    static func fadeOut(component: UIView, duration: Float) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {() in
            component.alpha = 0
        })
    }
    
    static func fadeOut(component: UIView, duration: Float, completion: @escaping (Bool) -> ()) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {() in
            component.alpha = 0
        }, completion: { (success) in
            completion(true)
        })
    }
    
}
