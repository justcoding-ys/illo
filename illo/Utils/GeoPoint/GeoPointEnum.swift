enum GeoPointEnum: Int {
    //    NOT_DETERMINED            : 아직 유저가 이 어플에 대한 권한을 부여하지 않을때
    
    //    DENIED                    : 로케이션서비스의 설정이 무효로 되어있음(유저에 의해서 해제됨.)
    //                                설정 > 프라이버시 > 위치정보 에서 위치정보 이용 허가해 주세요 다이얼로그
    
    //    RESTRICED                 : 위치정보 사용할수 없음(유저에 의한 권한 거절이 아님)
    //                                에러 다이얼로그
    
    //    AUTHORIZED_ALWAYS         : [허가받음]백그라운드에서도 위치정보 알아낼수 있음. 이 앱에선 사용하지 않는다.
    
    //    AUTHORIZED_WHEN_IN_USE    : [허가받음]실행 중에만 위치정보 알아낼수 있음. 이 앱은 이것을 사용함.
    
    case NOT_DETERMINED
    case DENIED
    case RESTRICED
    case AUTHORIZED_ALWAYS
    case AUTHORIZED_WHEN_IN_USE
}


