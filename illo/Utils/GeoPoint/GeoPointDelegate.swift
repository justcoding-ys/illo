import CoreLocation
import UIKit

protocol GeoPointDelegate: class {
    func didChangeAuthorization(status: GeoPointEnum) -> Void
    func didUpdateGeoPoint(angle: CGFloat, distance: Int) -> Void
}



