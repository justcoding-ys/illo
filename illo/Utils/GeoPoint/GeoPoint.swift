import CoreLocation
import UIKit

class GeoPoint: NSObject {
    weak var delegate: GeoPointDelegate?
    var locationManager: CLLocationManager!
    var destinationLocation: CLLocation!
    var currentLocation: CLLocation!
    
    private var isUpdating: Bool = false
    private var isHeading: Bool = false
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    }
    
    init(latitudeOfDestination: CLLocationDegrees, longitudeOfDestination: CLLocationDegrees) {
        super.init()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        setTargetLocation(latitudeOfTargetLocation: latitudeOfDestination, longitudeOfTargetLocation: longitudeOfDestination)
    }
    
    func setTargetLocation(latitudeOfTargetLocation: CLLocationDegrees, longitudeOfTargetLocation: CLLocationDegrees) -> Void {
        self.destinationLocation = CLLocation(latitude: latitudeOfTargetLocation, longitude: longitudeOfTargetLocation)
    }
    
    func setUpdateLocation(latitudeOfTargetLocation: CLLocationDegrees, longitudeOfTargetLocation: CLLocationDegrees) -> Void {
        stopGeoPoint()
        self.destinationLocation = CLLocation(latitude: latitudeOfTargetLocation, longitude: longitudeOfTargetLocation)
    }
    
    func startGeoPoint() {
        guard !isUpdating && !isHeading else { return }
        
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
        self.isHeading = true
        self.isUpdating = true
    }
    
    func stopGeoPoint() {
        guard isUpdating && isHeading else { return }
        locationManager.stopUpdatingHeading()
        locationManager.stopUpdatingLocation()
        self.isHeading = false
        self.isUpdating = false
    }
    
}

extension GeoPoint: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            self.delegate?.didChangeAuthorization(status: .NOT_DETERMINED)
            locationManager.requestWhenInUseAuthorization()
            break
        case .denied:
            self.delegate?.didChangeAuthorization(status: .DENIED)
            break
        case .restricted:
            self.delegate?.didChangeAuthorization(status: .RESTRICED)
            break
        case .authorizedAlways:
            self.delegate?.didChangeAuthorization(status: .AUTHORIZED_ALWAYS)
            break
        case .authorizedWhenInUse:
            startGeoPoint()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last!
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Erros: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if currentLocation == nil {
            self.stopGeoPoint()
            self.startGeoPoint()
            return
        }
        
        let angle = computeNewAngle(with: CGFloat(newHeading.trueHeading))
        
        let meters: CLLocationDistance = currentLocation.distance(from: destinationLocation!)
        
        self.delegate?.didUpdateGeoPoint(angle: angle, distance: Int(meters))
    }
    
    func computeNewAngle(with newAngle: CGFloat) -> CGFloat {
        //        let heading: CGFloat = {
        //            let originalHeading = (currentLocation?.bearingToLocationRadian(destinationLocation!))! - newAngle.toRadians
        //            switch UIDevice.current.orientation {
        //            case .faceDown: return -originalHeading
        //            default: return originalHeading
        //            }
        //        }()
        
        let heading: CGFloat = currentLocation.bearingToLocationRadian(destinationLocation) - newAngle.toRadians
        return CGFloat(orientationAdjustment().toRadians + heading)
    }
    
    private func orientationAdjustment() -> CGFloat {
        let isFaceDown: Bool = {
            switch UIDevice.current.orientation {
            case .faceDown: return true
            default: return false
            }
        }()
        
        let adjAngle: CGFloat = {
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:  return 90
            case .landscapeRight: return -90
            case .portrait, .unknown: return 0
            case .portraitUpsideDown: return isFaceDown ? 180 : -180
            }
        }()
        return adjAngle
    }
    
}

