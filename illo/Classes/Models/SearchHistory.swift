import Foundation

struct SearchHistory {
    let placeId: String
    let primaryAddress: String
    let fullAddress: String
    
    init(placeId: String, primaryAddress: String, fullAddress: String) {
        self.placeId = placeId
        self.primaryAddress = primaryAddress
        self.fullAddress = fullAddress
    }
    
    init?(dictionary: [String:String]) {
        guard let placeId = dictionary["placeId"],
            let primaryAddress = dictionary["primaryAddress"],
            let fullAddress = dictionary["fullAddress"] else { return nil }
        
        self.init(placeId: placeId, primaryAddress: primaryAddress, fullAddress: fullAddress)
    }
    
    var property: [String:String] {
        return ["placeId": self.placeId, "primaryAddress": self.primaryAddress, "fullAddress": self.fullAddress]
    }
}
