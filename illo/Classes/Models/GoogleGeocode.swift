import ObjectMapper
import CoreLocation

struct GoogleGeocode {
    var address_components = [AddressComponent]()
    var formatted_address = ""
    var place_id = ""
    var geometry = Geometry()
}

extension GoogleGeocode: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        address_components <- map["address_components"]
        formatted_address <- map["formatted_address"]
        place_id <- map["place_id"]
        geometry <- map["geometry"]
    }
}

extension GoogleGeocode {
    
    @discardableResult
    func getAddress() -> String {
        var address = [String]()

        for value in self.address_components {
            for type in value.types {
                if type == "country" || type == "postal_code" || type == "political" {
                    continue
                }
                address.append("\(value.long_name)")
            }
            
        }
        address = address.reduce([], {
            [$1] + $0
        })
        
        return address.joined(separator: " ")
    }
    
    @discardableResult
    func distanceFrom(lat: Double, lng: Double) -> String {
        var result = ""

        let distance = Int(CLLocation(latitude: self.geometry.lat, longitude: self.geometry.lng).distance(from: CLLocation(latitude: lat, longitude: lng)))
        
        if distance <= 1000 {
            result = "\(distance) m"
        } else if distance > 1000 {
            result = "\(distance/1000) km"
        } else if distance == 0 {
            result = R.string.localizable.currentLocation()
        }
        return result
    }
    func getDistanceText(distance: Int) -> String {
        var distanceText = ""
        distanceText = "\(distance) m"
        if distance > 1000 {
            distanceText = "\(distance/1000) km"
        } else if distance == 0 {
            distanceText = R.string.localizable.currentLocation()
        }
        
        return distanceText
    }
}

struct AddressComponent {
    var long_name = ""
    var short_name = ""
    var types = [String]()
}

extension AddressComponent: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        long_name <- map["long_name"]
        short_name <- map["short_name"]
        types <- map["types"]
    }
}

struct Geometry {
    var lat = Double()
    var lng = Double()
}

extension Geometry: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        lat <- map["location.lat"]
        lng <- map["location.lng"]
    }
}
