import Foundation
import UIKit

class PlaceSearchRouter: PlaceSearchWireframe {
    weak var viewController: UIViewController?
    
    static func assembleModule() -> UIViewController {
        let view = PlaceSearchViewController()
        let presenter = PlaceSearchPresenter()
        let interactor = PlaceSearchInteractor()
        let router = PlaceSearchRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
//        let navigation = UINavigationController(rootViewController: view)
//        navigation.isNavigationBarHidden = false
//        navigation.navigationBar.barStyle = .black
//        navigation.navigationBar.barTintColor = UIColor(hex: "FBFBFB")

        
        return view
    }
    
    func presentDestinationSearchView(googlePlaceId: String) {
        let rootViewController = viewController?.navigationController?.viewControllers.first
        if rootViewController is DestinationSearchViewController {
            let destinationView = rootViewController as! DestinationSearchViewController
            destinationView.googlePlaceId = googlePlaceId
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}

