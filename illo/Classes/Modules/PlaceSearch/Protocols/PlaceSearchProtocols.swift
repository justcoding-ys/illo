import Foundation
import UIKit

protocol PlaceSearchView: class {
    var presenter: PlaceSearchPresentation! { get set }
    
    func configureView()
    func configureNavigationBar(isAppeared: Bool)
    func reloadDataSearchHistoryView(histories: [SearchHistory])
//    self.placeInfoHistoryTableView.reloadDataWithArray(array
}

protocol PlaceSearchPresentation: class {
    weak var view: PlaceSearchView? { get set }
    var interactor: PlaceSearchUsecase! { get set }
    var router: PlaceSearchWireframe! { get set }
    
    func viewWillAppeared() -> Void
    func viewWillDisAppeared() -> Void
    func didSelectGMSAutocompleteRow(searchHistory: SearchHistory)
    func didSelectSearchHistoryRow(searchHistory: SearchHistory)
    func didDeleteSearchHistoryRow(searchHistory: SearchHistory)
}

protocol PlaceSearchUsecase: class {
    weak var output: PlaceSearchInteractorOutput! { get set }
    
    func storeSearchHistory(searchHistory: SearchHistory)
    func fetchSearchHistories()
    func deleteSearchHistory(searchHistory: SearchHistory)

}

protocol PlaceSearchInteractorOutput: class {
    func fetchedSearchHistories(result: [SearchHistory])
}

protocol PlaceSearchWireframe: class {
    weak var viewController: UIViewController? { get set }
    
    static func assembleModule() -> UIViewController
    func presentDestinationSearchView(googlePlaceId: String)
}
