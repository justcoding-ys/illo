import Foundation

class PlaceSearchPresenter {
    weak var view: PlaceSearchView?
    var interactor: PlaceSearchUsecase!
    var router: PlaceSearchWireframe!
}

extension PlaceSearchPresenter: PlaceSearchPresentation {
    
    
    
    func viewWillAppeared() {
        self.view?.configureNavigationBar(isAppeared: true)
        self.view?.configureView()
        self.interactor.fetchSearchHistories()
    }
    
    func viewWillDisAppeared() {
        self.view?.configureNavigationBar(isAppeared: false)
    }
    
    func didSelectGMSAutocompleteRow(searchHistory: SearchHistory) {
        self.interactor.storeSearchHistory(searchHistory: searchHistory)
        self.router.presentDestinationSearchView(googlePlaceId: searchHistory.placeId)
    }
    
    func didSelectSearchHistoryRow(searchHistory: SearchHistory) {
        self.router.presentDestinationSearchView(googlePlaceId: searchHistory.placeId)

    }
    
    func didDeleteSearchHistoryRow(searchHistory: SearchHistory) {
        self.interactor.deleteSearchHistory(searchHistory: searchHistory)
    }
    

}

extension PlaceSearchPresenter: PlaceSearchInteractorOutput {
    
    func fetchedSearchHistories(result: [SearchHistory]) {
        self.view?.reloadDataSearchHistoryView(histories: result)
    }
}
