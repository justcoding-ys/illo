import Foundation

class PlaceSearchInteractor: PlaceSearchUsecase {
    weak var output: PlaceSearchInteractorOutput!
    var data = [SearchHistory]()
    
    func storeSearchHistory(searchHistory: SearchHistory) {
        data.append(searchHistory)
        UserDefaults.standard.set(searchHistories: data, forKey: .searchHistory)
    }
    
    func fetchSearchHistories() {
        if let histories = UserDefaults.standard.object(forKey: .searchHistory) {
//                        UserDefaults.standard.removeObject(forKey: .searchHistory)
            data = histories
            self.output.fetchedSearchHistories(result: data.reversed())
        }
    }
    
    func deleteSearchHistory(searchHistory: SearchHistory) {
        if data.isEmpty {
            return
        }
        for (index, element) in data.enumerated() {
            if element.primaryAddress == searchHistory.primaryAddress &&
                element.fullAddress == searchHistory.fullAddress {
                data.remove(at: index)
                UserDefaults.standard.set(searchHistories: data, forKey: .searchHistory)
                return
            }
        }
    }
    
}
