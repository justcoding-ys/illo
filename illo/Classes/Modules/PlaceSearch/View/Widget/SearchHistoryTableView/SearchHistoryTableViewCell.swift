import UIKit

class SearchHistoryTableViewCell: UITableViewCell {
    var view: UIView! = UIView()
    var searchHistoryImage: UIImageView! = UIImageView()
    var primaryAddress: UILabel! = UILabel()
    var fullAddress: UILabel! = UILabel()
    
    var viewMargin: UIEdgeInsets! = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    var primaryAddressViewMargin: UIEdgeInsets! = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    var fullAddressViewMargin: UIEdgeInsets! = UIEdgeInsets(top: 4, left: 20, bottom: 0, right: 0)
    
    var searchHistory: SearchHistory! {
        didSet {
            primaryAddress.text = searchHistory.primaryAddress
            fullAddress.text = searchHistory.fullAddress
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(searchHistory: SearchHistory!) {
        self.searchHistory = searchHistory
    }
    
    private func configureConstraints() {
        self.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(viewMargin.top)
            make.bottom.equalToSuperview().offset(-viewMargin.bottom)
            make.left.equalToSuperview().offset(viewMargin.left)
            make.right.equalToSuperview().offset(-viewMargin.right)
        }
        
        view.addSubview(searchHistoryImage)
        searchHistoryImage.image = R.image.icon_search_pin_dark()
        searchHistoryImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
        }
        
        view.addSubview(primaryAddress)
        primaryAddress.font = UIFont.systemFont(ofSize: 17)
        primaryAddress.textColor = UIColor(hex: "424242")
        primaryAddress.snp.makeConstraints { (make) in
            make.top.equalTo(searchHistoryImage).offset(primaryAddressViewMargin.top)
            make.left.equalTo(searchHistoryImage.snp.right).offset(primaryAddressViewMargin.left)
        }
        
        view.addSubview(fullAddress)
        fullAddress.font = UIFont.systemFont(ofSize: 15)
        fullAddress.textColor = UIColor(hex: "757575")
        fullAddress.snp.makeConstraints { (make) in
            make.top.equalTo(primaryAddress.snp.bottom).offset(fullAddressViewMargin.top)
            make.left.equalTo(searchHistoryImage.snp.right).offset(fullAddressViewMargin.left)
            make.bottom.equalToSuperview().offset(-fullAddressViewMargin.bottom)
        }
        
    }
}
