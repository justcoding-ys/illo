import UIKit

protocol SearchHistoryTableViewDelegate: class {
    func didSelectRow(history: SearchHistory)
    func didDeleteRow(history: SearchHistory)
}

class SearchHistoryTableView: UITableView {
    var searchHistories = [SearchHistory]()
    var _delegate: SearchHistoryTableViewDelegate?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        register(SearchHistoryTableViewCell.self)
        self.separatorStyle = .none
        self.backgroundColor = UIColor(hex: "FBFBFB")
        
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension SearchHistoryTableView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchHistories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SearchHistoryTableViewCell
        let property = self.searchHistories[indexPath.row]
        
        cell.backgroundColor = UIColor(hex: "FBFBFB")
        cell.setup(searchHistory: property)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return self.rowheight
//    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        let value: SearchHistory = self.searchHistories[indexPath.row]
        _delegate?.didSelectRow(history: value)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self._delegate?.didDeleteRow(history: searchHistories[indexPath.row])
            searchHistories.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
        }
    }
    
    
    func reloadDataWithArray(_ array: [SearchHistory]) {
        self.searchHistories = array
        self.reloadData()
    }
}
