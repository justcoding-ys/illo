import UIKit
import GooglePlaces

class GMSAutocompleteTableViewCell: UITableViewCell {
    var view: UIView! = UIView()

    var primaryAddress: UILabel! = UILabel()
    var fullAddress: UILabel! = UILabel()
    var property: GMSAutocompletePrediction! {
        didSet {
            primaryAddress.text = property.attributedPrimaryText.string
            fullAddress.text = property.attributedFullText.string
        }
    }
    
    var margin: UIEdgeInsets! = UIEdgeInsets(top: 20, left: 20, bottom: 3, right: 3)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(_ property: GMSAutocompletePrediction!) {
        self.property = property
    }
    
    private func configureConstraints() {
        self.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(margin.top)
            make.bottom.equalToSuperview().offset(-margin.bottom)
            make.left.equalToSuperview().offset(margin.left)
            make.right.equalToSuperview().offset(-margin.right)
        }
        
        view.addSubview(primaryAddress)
        primaryAddress.font = UIFont.systemFont(ofSize: 16)
        primaryAddress.textColor = UIColor(hex: "383838")
        primaryAddress.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.centerY)
            make.left.right.equalToSuperview()
        }
        
        view.addSubview(fullAddress)
        fullAddress.font = UIFont.systemFont(ofSize: 14)
        fullAddress.textColor = UIColor(hex: "777777")
        fullAddress.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.centerY)
            make.left.right.equalToSuperview()
        }
    }
}
