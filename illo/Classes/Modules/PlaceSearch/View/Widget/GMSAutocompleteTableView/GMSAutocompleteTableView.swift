import UIKit
import GooglePlaces

protocol GMSAutocompleteTableViewDelegate: class {
    func didSelectRow(property: GMSAutocompletePrediction)
//    func didSelectRow(address: String)
}

class GMSAutocompleteTableView: UITableView {
    var resultArray = [GMSAutocompletePrediction]()
    var _delegate: GMSAutocompleteTableViewDelegate?
    var rowheight: CGFloat = 50.0
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .plain)
        register(GMSAutocompleteTableViewCell.self)
        self.separatorStyle = .none
        self.isScrollEnabled = false
        self.backgroundColor = UIColor(hex: "FBFBFB")
        
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension GMSAutocompleteTableView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as GMSAutocompleteTableViewCell
        let property = self.resultArray[indexPath.row]

//        cell.selectionStyle = .none
        cell.backgroundColor = UIColor(hex: "FBFBFB")
        cell.setup(property)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.rowheight
    }

    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        let value: GMSAutocompletePrediction = self.resultArray[indexPath.row]
        _delegate?.didSelectRow(property: value)
//        myDelegate?.didSelectRow(address: self.resultArray[indexPath.row].attributedFullText.string)
        //        self.presenter.didClickPlaceTable(self.resultsArray[indexPath.row].attributedFullText.string)
    }


    func reloadDataWithArray(_ array:[GMSAutocompletePrediction]){
        self.resultArray = array
        self.reloadData()
    }
}
