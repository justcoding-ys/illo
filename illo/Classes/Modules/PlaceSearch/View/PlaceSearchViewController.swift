import UIKit
import GooglePlaces

enum PlaceSearchViewClickedEventType: Int {
    case exit = 1, deleteHistory = 2
}


class PlaceSearchViewController: UIViewController {
    var presenter: PlaceSearchPresentation!
    
    var searchBar: UISearchBar! = UISearchBar()
    var navigationView: UIView! = UIView()
    var contentView: UIView! = UIView()
    
    var gmsAutocompleteTableView: GMSAutocompleteTableView! = GMSAutocompleteTableView()
    var gmsFetcher: GMSAutocompleteFetcher!
    var poweredbyImage: UIImageView! = UIImageView()
    
    
    var searchHistoryTitleView: UILabel! = UILabel()
    var searchHistoryTableView: SearchHistoryTableView! = SearchHistoryTableView()
        
    override func viewWillAppear(_ animated: Bool) {
        self.presenter.viewWillAppeared()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.presenter.viewWillDisAppeared()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: UITouch in touches {

            let tag = PlaceSearchViewClickedEventType(rawValue: touch.view!.tag)
            switch tag {
            case .exit?:
//                self.navigationController?.popViewController(animated: true)
                self.navigationController?.popToRootViewController(animated: true)
            case .deleteHistory?:
                print("history delete")
            case .none: break
            }
        }
    }
    
    
}

extension PlaceSearchViewController {
    func configureNavigationView() {
        self.view.addSubview(navigationView)
        navigationView.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.height.equalTo((navigationController?.navigationBar.frame.height)!)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        let navigationItemExit = UIImageView()
        navigationItemExit.image = R.image.icon_navigation_exit()
        navigationItemExit.tag = PlaceSearchViewClickedEventType.exit.rawValue
        navigationItemExit.isUserInteractionEnabled = true
        
        navigationView.addSubview(navigationItemExit)
        navigationItemExit.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(20)
        }
        
        let navigationItemTitle = UILabel()
        navigationItemTitle.text = R.string.localizable.placeSearchNavigationTitle()
        navigationItemTitle.textColor = UIColor(hex: "424242")
        navigationItemTitle.textAlignment = .left
        navigationItemTitle.font = UIFont.systemFont(ofSize: 18)
        
        navigationView.addSubview(navigationItemTitle)
        navigationItemTitle.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    func configureContentView() {
        self.view.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(navigationView.snp.bottom)
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        configureGMSAutocompleteView()
        configurePlaceInfoHistoryView()
    }
    
    private func configureGMSAutocompleteView() {
        configureGMSAutocompleteSearchView()
        configureGMSAutocompleteTableView()
    }
    
    private func configureGMSAutocompleteSearchView() {
        let filter: GMSAutocompleteFilter = GMSAutocompleteFilter()
        filter.type = .noFilter
        gmsFetcher = GMSAutocompleteFetcher(bounds: nil, filter: filter)
        gmsFetcher.delegate = self
        
        searchBar.showsCancelButton = false
        searchBar.placeholder = R.string.localizable.placeSearchPlaceHolder()
        searchBar.backgroundImage = UIImage()
        searchBar.tintColor = UIColor(hex: "424242")
        searchBar.delegate = self
        
        for subView in searchBar.subviews {
            for secondSubView in subView.subviews {
                if secondSubView is UITextField {
                    let textField = secondSubView as! UITextField
                    textField.backgroundColor = UIColor(hex: "EDEDEE").withAlphaComponent(0.5)
                    textField.font = UIFont.systemFont(ofSize: 13)
                }
            }
        }
        
        self.contentView.addSubview(searchBar)
        searchBar.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.height.equalTo((navigationController?.navigationBar.frame.height)!)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        poweredbyImage.image = R.image.powered_by_google_on_white()
        self.contentView.addSubview(poweredbyImage)
        poweredbyImage.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.right.equalToSuperview().offset(-20)
        }
    }
    
    private func configureGMSAutocompleteTableView() {
        self.contentView.addSubview(gmsAutocompleteTableView)
        
        gmsAutocompleteTableView.snp.makeConstraints { (make) in
            make.top.equalTo(poweredbyImage.snp.bottom)
            make.width.left.right.equalToSuperview()
            make.height.equalTo(gmsAutocompleteTableView.rowheight * 5)
            make.centerX.equalToSuperview()
        }
        gmsAutocompleteTableView._delegate = self
    }
    
    private func configurePlaceInfoHistoryView() {
        let historyViewMargin = UIEdgeInsets(top: 30, left: 20, bottom: 0, right: 20)
        
        self.contentView.addSubview(searchHistoryTitleView)
        searchHistoryTitleView.text = R.string.localizable.placeSearchHistoryTitle()
        searchHistoryTitleView.font = UIFont.systemFont(ofSize: 14)
        searchHistoryTitleView.textColor = UIColor(hex: "B3B3B3")
//        placeInfoHistoryTitleView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        searchHistoryTitleView.snp.makeConstraints { (make) in
            make.top.equalTo(gmsAutocompleteTableView.snp.bottom).offset(historyViewMargin.top)
            make.left.equalToSuperview().offset(historyViewMargin.left)
//            make.right.equalToSuperview().offset(-historyViewMargin.right)
        }
        self.contentView.addSubview(searchHistoryTableView)
        
        searchHistoryTableView.snp.makeConstraints { (make) in
            make.top.equalTo(searchHistoryTitleView.snp.bottom)
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(historyViewMargin.left)
            make.right.equalToSuperview().offset(-historyViewMargin.right)
        }
        
        searchHistoryTableView._delegate = self
    }
    
}

extension PlaceSearchViewController : PlaceSearchView {
    func configureView() {
        self.view.backgroundColor = UIColor(hex: "FBFBFB")
        
        configureNavigationView()
        configureContentView()
    }
    
    func configureNavigationBar(isAppeared: Bool) {
        if case isAppeared = true {
            navigationController?.navigationBar.isHidden = true
            navigationController?.navigationBar.barStyle = .default
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            return
        }
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barStyle = .black
        
    }
    
    func reloadDataSearchHistoryView(histories: [SearchHistory]) {
        self.searchHistoryTableView.reloadDataWithArray(histories)
    }
}


extension PlaceSearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.gmsFetcher.sourceTextHasChanged(searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}

extension PlaceSearchViewController: GMSAutocompleteTableViewDelegate {
    func didSelectRow(property: GMSAutocompletePrediction) {
        self.presenter.didSelectGMSAutocompleteRow(searchHistory:
            SearchHistory(placeId: property.placeID!,
                          primaryAddress: property.attributedPrimaryText.string,
                          fullAddress: property.attributedFullText.string))
    }
    
    func reloadDataWithArray(_ array:[GMSAutocompletePrediction]){
        self.gmsAutocompleteTableView.reloadDataWithArray(array)
    }

}

extension PlaceSearchViewController: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.reloadDataWithArray([GMSAutocompletePrediction]())
        
        for prediction in predictions {
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.gmsAutocompleteTableView.resultArray.append(prediction)
                self.reloadDataWithArray(self.gmsAutocompleteTableView.resultArray)
            }
        }
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error.localizedDescription)
    }
}

extension PlaceSearchViewController: SearchHistoryTableViewDelegate {
    func didSelectRow(history: SearchHistory) {
        self.presenter.didSelectSearchHistoryRow(searchHistory: history)
    }
    
    func didDeleteRow(history: SearchHistory) {
        self.presenter.didDeleteSearchHistoryRow(searchHistory: history)
    }
    
    func reloadDataWithArray(_ array: [SearchHistory]) {
        self.searchHistoryTableView.reloadDataWithArray(array)
//        self.gmsAutocompleteTableView.reloadDataWithArray(array)
    }
}
