import Foundation
import UIKit

protocol IlloNaviSegmentedWireframe: class {
    weak var viewController: UIViewController? { get set }
    
    static func assembleModule(geocode: GoogleGeocode) -> UIViewController
}

