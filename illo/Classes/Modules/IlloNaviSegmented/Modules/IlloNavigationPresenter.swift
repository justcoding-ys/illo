import UIKit
import GooglePlaces

class IlloNavigationPresenter {
    weak var view: IlloNavigationView?
    var interactor: IlloNavigationUsecase!
    var router: IlloNavigationWireframe!
}

extension IlloNavigationPresenter: IlloNavigationPresentation {
    func viewDidLoaded() {
        self.view?.configureView(geocode: self.interactor.fetchGeocode())
    }
}

extension IlloNavigationPresenter: IlloNavigationInteractorOutput {
    
}
