import UIKit
import GoogleMaps

class IlloNaviMapViewController: UIViewController {
    var googleMap: GMSMapView = GMSMapView()
    var locationManager: CLLocationManager = CLLocationManager()
    var geocode: GoogleGeocode!
//    var destination: CLLocationCoordinate2D!
    var destinationMarker: GMSMarker!
    
    init(googleGeocode: GoogleGeocode) {
        self.geocode = googleGeocode
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureGoogleMapView()
    }
    
    func configureGoogleMapView() {
        googleMap.delegate = self
        googleMap.isMyLocationEnabled = true
        //        googleMap.settings.compassButton = true
        googleMap.settings.indoorPicker = true
        //        googleMap.settings.tiltGestures = false
        //        googleMap.settings.scrollGestures = false
        googleMap.settings.rotateGestures = false
        //        googleMap.settings.zoomGestures = false
        //        googleMap.settings.myLocationButton = true
        
        destinationMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: self.geocode.geometry.lat, longitude: self.geocode.geometry.lng))
        destinationMarker.icon = R.image.icon_map_pin()
        destinationMarker.map = self.googleMap
        
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        
        view.addSubview(googleMap)
        googleMap.snp.makeConstraints { (make) in
            make.size.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension IlloNaviMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    }
}

extension IlloNaviMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        let camera = GMSCameraPosition.camera(withLatitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!, zoom: 18)
        
        let bounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate: (locations.last?.coordinate)!, coordinate: CLLocationCoordinate2D(latitude: self.geocode.geometry.lat, longitude: self.geocode.geometry.lng))
        
        
        //        googleMap.animate(to: camera)
        googleMap.moveCamera(GMSCameraUpdate.fit(bounds, withPadding: 150))
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        googleMap.animate(toBearing: newHeading.trueHeading)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .denied:
            print("ERROR DENIED")
            break
        case .restricted:
            print("ERROR")
            break
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            self.locationManager.startUpdatingHeading()
            break
        case .authorizedAlways:
            break
        }
    }
    
}

