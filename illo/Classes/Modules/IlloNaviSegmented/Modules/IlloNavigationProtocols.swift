import Foundation
import UIKit

protocol IlloNavigationView: class {
    var presenter: IlloNavigationPresentation! { get set }
    
    func configureView(geocode: GoogleGeocode)
    func configureGeopoint(geocode: GoogleGeocode)
}

protocol IlloNavigationPresentation: class {
    weak var view: IlloNavigationView? { get set }
    var interactor: IlloNavigationUsecase! { get set }
    var router: IlloNavigationWireframe! { get set }
    
    func viewDidLoaded() -> Void
}

protocol IlloNavigationUsecase: class {
    weak var output: IlloNavigationInteractorOutput! { get set }
    var destination: GoogleGeocode! { get set }
    
    func fetchGeocode() -> GoogleGeocode
}

protocol IlloNavigationInteractorOutput: class {
    
}

protocol IlloNavigationWireframe: class {
    weak var viewController: UIViewController? { get set }
    
    static func assembleModule(googleGeocode: GoogleGeocode) -> UIViewController
}
