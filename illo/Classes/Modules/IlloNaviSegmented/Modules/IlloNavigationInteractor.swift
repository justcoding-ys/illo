import UIKit

class IlloNavigationInteractor {
    weak var output: IlloNavigationInteractorOutput!
    var destination: GoogleGeocode!
}

extension IlloNavigationInteractor: IlloNavigationUsecase {
    func fetchGeocode() -> GoogleGeocode {
        return destination
    }
    
}
