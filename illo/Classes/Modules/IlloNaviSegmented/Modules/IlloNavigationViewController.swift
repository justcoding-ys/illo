import FBAudienceNetwork
import UIKit

class IlloNavigationViewController: UIViewController {
    var presenter: IlloNavigationPresentation!
    
    var navigationView: UIView! = UIView()
    var contentView: UIImageView = UIImageView()
    
    var adView: UIView! = UIView()
    var nativeAd: FBNativeAd!
    var nativeFBAdView: NativeFBAdView!
    
    var indicatorContainerView: UIView! = UIView()
    var illoNaviIndicatorView: IlloNaviIndicatorView! = IlloNaviIndicatorView()
//    var indicatorView: IlloNaviIndicatorView! = IlloNaviIndicatorView()
    var geoPoint: GeoPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoaded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureAdView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        nativeAd = nil
    }
    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for touch: UITouch in touches {
//            print(touch.view!.tag)
//            let tag = DestinationSearchViewClickedEventType(rawValue: touch.view!.tag)
//            switch tag {
//            case .current?:
//                self.presenter.didClickedCurrentIcon()
//            case .placeInfo?:
//                self.presenter.didClickedPlaceInfo()
//            case .share?:
//                self.presenter.didClickedMenuShare()
//            case .start?:
//                self.presenter.didClickedMenuStart(googleGeocode: self.mapInfoView.mapInfo?.googleGeocode)
//            case .none: break
//            }
//        }
//    }
}

extension IlloNavigationViewController: IlloNavigationView {
    
    func configureView(geocode: GoogleGeocode) {
        self.view.backgroundColor = UIColor(hex: "424242")
        
        self.view.addSubview(contentView)
        contentView.backgroundColor = UIColor(hex: "FBFBFB")
        contentView.image = R.image.image_directionnavi_background()
        contentView.contentMode = .scaleAspectFit
        contentView.blurEffect(.light)
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        configureIndicatorContainerView(geocode: geocode)
//        configureAdView()
    }
    
    func configureGeopoint(geocode: GoogleGeocode) {
        guard self.geoPoint == nil else {
            return
        }
        self.geoPoint = GeoPoint(latitudeOfDestination: geocode.geometry.lat, longitudeOfDestination: geocode.geometry.lng)
        self.geoPoint?.delegate = self
    }
}

extension IlloNavigationViewController {
    
    private func configureIndicatorContainerView(geocode: GoogleGeocode) {
        self.contentView.addSubview(self.indicatorContainerView)
        
        self.indicatorContainerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.65)
//            make.bottom.equalTo(self.adView.snp.top)
            make.width.left.right.equalToSuperview()
        }
        
        self.indicatorContainerView.addSubview(illoNaviIndicatorView)
        illoNaviIndicatorView.snp.makeConstraints { (make) in
//            make.center.equalToSuperview()
            make.top.bottom.left.right.equalToSuperview()
        }
        self.illoNaviIndicatorView.setAddress(address: geocode.getAddress())
        configureGeopoint(geocode: geocode)
    }
    
    private func configureAdView() {
        self.contentView.addSubview(self.adView)
        self.adView.backgroundColor = UIColor(hex: "FBFBFB")
        self.adView.snp.makeConstraints { (make) in
            make.top.equalTo(self.indicatorContainerView.snp.bottom)
            make.width.left.right.bottom.equalToSuperview()
        }
        
        if self.nativeAd == nil {
            setFBAudienceNetwork()
        }
    }
    
    private func setFBAudienceNetwork() {
        FBAdSettings.setLogLevel(FBAdLogLevel.error)

//        let banner = FBAdView(placementID: "229887310920807_229921794250692", adSize: kFBAdSizeHeight50Banner, rootViewController: self)
//        self.adView.addSubview(banner)
//        banner.snp.makeConstraints { (make) in
//            make.center.equalToSuperview()
//            make.width.equalTo(banner.bounds.size.width)
//            make.height.equalTo(banner.bounds.size.height)
//        }
//        banner.loadAd()
        
//        FBAdSettings.clearTestDevices()
        FBAdSettings.addTestDevice("04c083b5bdfcbf38d6e1c35a9744461329cf455a")
        self.nativeAd = FBNativeAd(placementID: "229887310920807_229888404254031")
        self.nativeAd.delegate = self
        self.nativeAd.mediaCachePolicy = .all
        
        self.nativeAd.load()
    }
    
    private func refreshNativeAd() {
        if(self.nativeAd != nil && self.nativeFBAdView != nil) {
            self.nativeAd.unregisterView()
            self.nativeFBAdView.removeFromSuperview()
            self.nativeFBAdView = nil
            self.nativeAd = nil

            setFBAudienceNetwork()
        }
    }

}

extension IlloNavigationViewController: GeoPointDelegate {
    func didChangeAuthorization(status: GeoPointEnum) -> Void {
        switch status {
        case .DENIED:
            break;
        case .RESTRICED:
            break;
        default:
            break;
        }
    }
    
    func didUpdateGeoPoint(angle: CGFloat, distance: Int) -> Void {
        UIView.animate(withDuration: 0.2) {
            self.illoNaviIndicatorView.indicatorImage.transform = CGAffineTransform(rotationAngle: angle)
//            self.illoNaviIndicatorView.transformImage(angle: angle)
        }
        
        distance >= 1000 ? self.illoNaviIndicatorView.changeDistance(distance: "\(distance / 1000) km") : self.illoNaviIndicatorView.changeDistance(distance: "\(distance) m")
        
    }
    
}

extension IlloNavigationViewController: FBNativeAdDelegate {
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        if self.nativeAd != nil {
            self.nativeAd.unregisterView()
        }
        self.nativeAd = nativeAd
        for view in self.adView.subviews {
            view.removeFromSuperview()
        }
        
        self.nativeFBAdView = NativeFBAdView()
        self.adView.addSubview(self.nativeFBAdView)

        self.nativeFBAdView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.width.height.equalToSuperview()
            make.center.equalToSuperview()
            //            make.bottom.equalToSuperview()
        }
        self.nativeFBAdView.setup(nativeAd: nativeAd)

        self.nativeAd.registerView(forInteraction: self.nativeFBAdView, with: self)
        self.nativeFBAdView.setNeedsFocusUpdate()
//        self.nativeAd.registerView(forInteraction: self.nativeFBAdView, with: self, withClickableViews: [self.nativeFBAdView.actionBtn, self.nativeFBAdView.contentView])
    }
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        NSLog("Ad Failed: %@", error.localizedDescription)
    }
    
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        print(nativeAd)
    }
    
    func nativeAdDidFinishHandlingClick(_ nativeAd: FBNativeAd) {
        refreshNativeAd()
    }
    
}
