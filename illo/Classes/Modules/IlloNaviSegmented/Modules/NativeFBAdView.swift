import UIKit
import FBAudienceNetwork

class NativeFBAdView: UIView {
    
    var headerView: UIView = UIView()
    var contentView: UIView = UIView()
    var footerView: UIView = UIView()
    
    var adIcon: UIImageView = UIImageView()
    var title: UILabel = UILabel()
    var sponsored: UILabel = UILabel()
    var choiceView: FBAdChoicesView = FBAdChoicesView()
    
    var cover: FBMediaView = FBMediaView()
    var socialContext: UILabel = UILabel()
    
    var actionBtn: UIButton = UIButton()
    var body: UILabel = UILabel()
    
    var marginLeft = 15
    var marginRight = -15
    var nativeBackgroundColor: UIColor = UIColor.clear
//        UIColor(hex: "0E1C23").withAlphaComponent(0.1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(headerView)
        self.headerView.addSubview(adIcon)
        self.headerView.addSubview(title)
        self.headerView.addSubview(sponsored)
        self.headerView.addSubview(choiceView)
        
        self.addSubview(contentView)
        self.contentView.addSubview(cover)
        
        self.addSubview(footerView)
        //        self.footerView.addSubview(socialContext)
        self.footerView.addSubview(body)
        self.footerView.addSubview(actionBtn)
        
        backgroundColor = self.nativeBackgroundColor
        configureConstraints()
    }
    
    func setup(nativeAd: FBNativeAd) {
        nativeAd.icon?.loadAsync(block: { (resultImage) in
            self.adIcon.image = resultImage
        })
        title.text = nativeAd.title
        self.title.font = UIFont.systemFont(ofSize: 13)
        sponsored.text = "Sponsored"
        self.sponsored.font = UIFont.systemFont(ofSize: 13)
        choiceView.nativeAd = nativeAd
        
        //        choiceView.backgroundColor = UIColor(hex: "D8D8D8")
        
        cover.nativeAd = nativeAd
        cover.clipsToBounds = true
        //        nativeAd.coverImage?.loadAsync(block: { (resultImage) in
        //            self.cover.nativeAd.coverImage = result
        //        })
        
        socialContext.text = nativeAd.socialContext
        self.socialContext.font = UIFont.systemFont(ofSize: 13)
        body.text = nativeAd.body
        self.body.font = UIFont.systemFont(ofSize: 13)
        choiceView.nativeAd?.adChoicesIcon?.loadAsync(block: { (resultImage) in
            self.choiceView.backgroundColor = UIColor.clear.withAlphaComponent(0.0)
            self.choiceView.label?.textColor = UIColor.black
        })
        actionBtn.layer.cornerRadius = 10
        actionBtn.setTitle(nativeAd.callToAction, for: .normal)
        actionBtn.backgroundColor = UIColor(hex: "FDD835")
        actionBtn.titleLabel?.textColor = UIColor(hex: "424242")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureConstraints() {
        
        self.headerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.2)
            make.centerX.equalToSuperview()
        }
        
        self.adIcon.snp.makeConstraints { (make) in
            make.width.equalTo(headerView.snp.height).multipliedBy(0.6)
            make.height.equalTo(headerView.snp.height).multipliedBy(0.6)
            make.left.equalToSuperview().offset(marginLeft)
            make.centerY.equalToSuperview()
        }
        
        //        self.title.textColor = UIColor.white
        self.title.snp.makeConstraints { (make) in
            make.top.equalTo(adIcon.snp.top)
            make.left.equalTo(adIcon.snp.right).offset(10)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(adIcon.snp.height).dividedBy(2)
        }
        //
        //        self.sponsored.textColor = UIColor.white
        self.sponsored.snp.makeConstraints { (make) in
            make.top.equalTo(title.snp.bottom)
            make.left.equalTo(adIcon.snp.right).offset(10)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(adIcon.snp.height).dividedBy(2)
        }
        //
        self.choiceView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(marginRight)
            make.width.equalTo(self.headerView.snp.width).multipliedBy(0.2)
            make.height.equalTo(self.headerView.snp.height).multipliedBy(0.6)
        }
        //
        self.contentView.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerView.snp.bottom)
            make.height.equalToSuperview().multipliedBy(0.6)
            make.left.equalToSuperview().offset(marginLeft)
            make.right.equalToSuperview().offset(marginRight)
            make.centerX.equalToSuperview()
        }
        self.cover.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        //
        self.footerView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.bottom)
            make.height.equalToSuperview().multipliedBy(0.2)
            make.left.equalToSuperview().offset(marginLeft)
            make.right.equalToSuperview().offset(marginRight)
            make.bottom.equalToSuperview()
        }
        
        //        self.socialContext.textColor = UIColor.white
        //        self.socialContext.baselineAdjustment = .alignCenters
        //        self.socialContext.snp.makeConstraints { (make) in
        //            make.top.equalToSuperview()
        //            make.width.equalToSuperview().multipliedBy(0.7)
        //            make.height.equalToSuperview().dividedBy(2)
        //            make.left.equalToSuperview()
        //        }
        //
        self.body.baselineAdjustment = .alignCenters
        //        self.body.textColor = UIColor.white
        self.body.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalToSuperview()
            make.left.equalToSuperview()
        }
        //
        actionBtn.setTitleColor(UIColor.black, for: .normal)
        
        self.actionBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(body.snp.right)
            make.right.equalToSuperview()
        }
    }
    
}

