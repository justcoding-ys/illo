import Foundation
import UIKit

class IlloNavigationRouter: IlloNavigationWireframe {
    
    weak var viewController: UIViewController?
    
    static func assembleModule(googleGeocode: GoogleGeocode) -> UIViewController {
        let view = IlloNavigationViewController()
        let presenter = IlloNavigationPresenter()
        let interactor = IlloNavigationInteractor()
        let router = IlloNavigationRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.destination = googleGeocode
        
        router.viewController = view
        
//        let navigation = UINavigationController(rootViewController: view)
//        navigation.isNavigationBarHidden = false
        
        return view
    }
    
//    func presentDestinationSearchView(googlePlaceId: String) {
//        let rootViewController = viewController?.navigationController?.viewControllers.first
//        if rootViewController is DestinationSearchViewController {
//            let destinationView = rootViewController as! DestinationSearchViewController
//            destinationView.googlePlaceId = googlePlaceId
//            viewController?.navigationController?.popToRootViewController(animated: true)
//        }
//    }
    
}


