import Foundation
import UIKit

class IlloNaviIndicatorView: UIView {
    var indicatorView: UIView! = UIView()
    var indicatorImage: UIImageView! = UIImageView()
    
    var infoView: UIView! = UIView()
    var addressLabel: UILabel! = UILabel()
    var distanceLabel: UILabel! = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureConstraints() {
        self.addSubview(indicatorView)
        self.indicatorView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.8)
        }
        self.indicatorView.addSubview(self.indicatorImage)
        self.indicatorImage.image = R.image.icon_direction_indicator()
        self.indicatorImage.contentMode = .scaleAspectFill
//        self.indicatorImage.backgroundColor = UIColor.red.withAlphaComponent(0.4)
        self.indicatorImage.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        self.addSubview(infoView)
        self.infoView.snp.makeConstraints { (make) in
            make.top.equalTo(self.indicatorView.snp.bottom)
            make.left.right.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.infoView.addSubview(addressLabel)
        self.addressLabel.font = UIFont.systemFont(ofSize: 17)
        self.addressLabel.textAlignment = .center
        self.addressLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        self.infoView.addSubview(distanceLabel)
        self.distanceLabel.font = UIFont.boldSystemFont(ofSize: 30)
        self.distanceLabel.textAlignment = .center
        self.distanceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.addressLabel.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
        }
    }
    
    func changeDistance(distance: String) {
        self.distanceLabel.text = distance
    }
    
    func setAddress(address: String) {
        self.addressLabel.text = address
    }
    
    func transformImage(angle: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.indicatorImage.transform = CGAffineTransform(rotationAngle: angle)
        }
    }
}
