import Foundation
import UIKit

class IlloNaviSegmentedRouter: IlloNaviSegmentedWireframe {
    weak var viewController: UIViewController?
    
    static func assembleModule(geocode: GoogleGeocode) -> UIViewController {
        return IlloNaviSegmentedController(geocode: geocode)
    }
}
