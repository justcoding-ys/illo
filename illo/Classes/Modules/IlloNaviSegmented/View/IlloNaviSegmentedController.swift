import UIKit
import GoogleMaps

class IlloNaviSegmentedController: UIViewController {
    enum TabIndex: Int {
        case IlloNaviView = 0, IlloNaviMapView
    }
    var geocode: GoogleGeocode!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(items: ["Illo", "Map"])
    var contentView: UIView = UIView()
    
    var currentViewController: UIViewController?
    
    lazy var illoNaviView: UIViewController? = {
        let view = IlloNavigationViewController()
        let presenter = IlloNavigationPresenter()
        let interactor = IlloNavigationInteractor()
        let router = IlloNavigationRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.destination = geocode
        
        router.viewController = view
        
        return view
//        return IlloNavigationRouter.assembleModule(googleGeocode: geocode)
    }()
    lazy var illoNaviMapView: UIViewController? = {
//        let view = DirectionMapViewController()
//        view.setup(destination: destination!)
        
        return IlloNaviMapViewController(googleGeocode: geocode)
    }()
    
    init(geocode: GoogleGeocode) {
        self.geocode = geocode
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func backAction() {
        self.navigationController?.popToRootViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }

    private func configureView() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: R.image.icon_navigation_back(), style: .plain, target: self, action: #selector(IlloNaviSegmentedController.backAction))
        self.view.addSubview(contentView)
        
        segmentedControl.selectedSegmentIndex = TabIndex.IlloNaviView.rawValue
        
        //        segmentedControl.removeBorders()
        
        navigationItem.titleView = segmentedControl
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.left.equalToSuperview()
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        segmentedControl.addTarget(self, action: #selector(IlloNaviSegmentedController.segmentedControlViewChanged(_:)), for: .touchUpInside)
        segmentedControl.addTarget(self, action: #selector(IlloNaviSegmentedController.segmentedControlViewChanged(_:)), for: .valueChanged)
        
        //        segmentedControl.setup()
        
        displayCurrentTab(TabIndex.IlloNaviView.rawValue)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func displayCurrentTab(_ tabIndex: Int) {
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            self.contentView.addSubview(vc.view)
            vc.view.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.left.equalToSuperview()
                make.right.equalToSuperview()
            })
            self.currentViewController = vc
            
        }
    }
    
    private func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.IlloNaviView.rawValue:
            vc = illoNaviView
        case TabIndex.IlloNaviMapView.rawValue:
            vc = illoNaviMapView
        default:
            return nil
        }
        
        return vc
    }
    
    @objc func segmentedControlViewChanged(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
}

