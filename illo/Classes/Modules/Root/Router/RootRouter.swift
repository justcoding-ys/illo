import GoogleMaps
import GooglePlaces

class RootRouter: RootWireframe {
    
    let GOOGLE_API_KEY = Constant.GoogleAPI.API_KEY
    
    var coordinate: CLLocationCoordinate2D?
    
    init() {
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
    }
    
    init(coordinate: CLLocationCoordinate2D?) {
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        self.coordinate = coordinate
    }
    
    func presentSplashScreen(in window: UIWindow) {
        window.makeKeyAndVisible()
        window.rootViewController = SplashRouter.assembleModule()
    }
    
}


