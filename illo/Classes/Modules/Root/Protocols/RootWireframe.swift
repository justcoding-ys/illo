import UIKit
//import GoogleMaps

protocol RootWireframe: class {
    func presentSplashScreen(in window: UIWindow) -> Void
}
