import Foundation

class DestinationSearchInteractor: DestinationSearchUsecase {
    weak var output: DestinationSearchInteractorOutput!
    
    func requestCoreLocation() {
        LocationManager.getInstance().configure(delegate: self)
        LocationManager.getInstance().getLocationManager().requestWhenInUseAuthorization()
    }
    
    func fetchCurrentLocation() {
        LocationManager.getInstance().getLocationManager().startUpdatingLocation()
    }
    
    func fetchGeocodeFromPlaceId(googlePlaceId: String) {
        GoogleGeocodeDataManager.shared.fetchGeocodeFromPlaceId(googlePlaceId).subscribe(
            onNext: { (result) in
                if result != nil {
                    self.output.fetchedGeocodeFromPlaceId(result!)
                }
        }, onError: { (error) in
                self.output.fetchedError(error)
        }, onCompleted: {})
    }
    
    func fetchGeocode(_ latitude: Double, _ longitude: Double) {
        GoogleGeocodeDataManager.shared.fetchGeocodeFromLatlng(latitude, longitude).subscribe(
            onNext: { (result) in
                if result != nil {
                    self.output.fetchedGeocode(result!)
                }
        }, onError: { (error) in
            self.output.fetchedError(error)
        }, onCompleted: {})
    }
    
}

extension DestinationSearchInteractor: LocationManagerDelegate {
    func didChangeAuthorization(status: LocationManagerEnum) {
        if status == .AUTHORIZED_WHEN_IN_USE || status == .AUTHORIZED_ALWAYS {
            output.validatedCoreLocation()
        }
    }
    
    func didUpdateLocations(latitude: Double, longitude: Double) {
        LocationManager.getInstance().getLocationManager().stopUpdatingLocation()
        output.fetchedCurrenLocation(latitude, longitude)
    }
    
}
