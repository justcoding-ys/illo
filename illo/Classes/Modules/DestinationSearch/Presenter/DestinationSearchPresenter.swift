import Foundation
import CoreLocation

class DestinationSearchPresenter {
    weak var view: DestinationSearchView?
    var interactor: DestinationSearchUsecase!
    var router: DestinationSearchWireframe!
}

extension DestinationSearchPresenter: DestinationSearchPresentation {
    func viewDidLoad() {
        view?.configureNavigationBar()
        
    }
    
    func viewWillAppeared(googlePlaceId: String?) {
        guard googlePlaceId == nil else {
            interactor.fetchGeocodeFromPlaceId(googlePlaceId: googlePlaceId!)
            return
        }
        interactor.requestCoreLocation()
        
    }
    
    func didClickedMenuStart(googleGeocode: GoogleGeocode!) {
        router.presentIlloNavigation(googleGeocode: googleGeocode)
    }
    
    func didClickedMenuShare() {
        print("did clicked Share")
    }
    
    func didClickedPlaceInfo() {
        print("did clicked PlaceInfo")
    }
    
    func didClickedSearchBar() {
        router.presentPlaceSearch()
    }
    
    func didClickedCurrentIcon() {
        interactor.fetchCurrentLocation()
    }
    
    func didMovedGoogleMap(_ latitude: Double, _ longitude: Double) {
        interactor.fetchGeocode(latitude, longitude)
    }

}

extension DestinationSearchPresenter: DestinationSearchInteractorOutput {    
    func validatedCoreLocation() {
        view?.configureGoogleMapView()
        interactor.fetchCurrentLocation()
    }
    
    func fetchedCurrenLocation(_ latitude: Double, _ longitude: Double) {
        view?.animateGoogleMapLocation(latitude, longitude)
        view?.configureMapInfoView()
        view?.configureMapMarkerView()
    }
    
    func fetchedGeocode(_ geocode: GoogleGeocode) {
        view?.animateMapInfoView(geocode)
        view?.animateMapMarkerView()
    }
    
    func fetchedGeocodeFromPlaceId(_ geocode: GoogleGeocode) {
        view?.animateGoogleMapLocation(geocode.geometry.lat, geocode.geometry.lng)
        fetchedGeocode(geocode)
    }
    
    func fetchedError(_ error: Error) {
        print(error.localizedDescription)
    }
}

//extension DestinationSearchPresenter: LocationManagerDelegate {
//    func didChangeAuthorization(status: LocationManagerEnum) {
////        if status == .DENIED {
////
////        }
////
////        if status == .RESTRICED {
////
////        }
//        if status == .AUTHORIZED_WHEN_IN_USE || status == .AUTHORIZED_ALWAYS{
//            view?.configureGoogleMapView()
//        }
//    }
//
//    func didUpdateLocations(latitude: Double, longitude: Double) {
//        view?.configureDestinationInfoView()
//    }
//
//}

