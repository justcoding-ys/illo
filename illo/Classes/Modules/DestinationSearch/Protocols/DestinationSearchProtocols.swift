import UIKit
import CoreLocation

protocol DestinationSearchView: class {
    var presenter: DestinationSearchPresentation! { get set}
    
    func configureNavigationBar()
    
    func configureGoogleMapView()
    func animateGoogleMapLocation(_ latitude: Double, _ longitude: Double)
    
    func configureMapInfoView()
    func configureMapMarkerView()
    func animateMapInfoView(_ geocode: GoogleGeocode)
    func animateMapMarkerView()
}

protocol DestinationSearchPresentation: class {
    weak var view: DestinationSearchView? { get set }
    var interactor: DestinationSearchUsecase! { get set }
    var router: DestinationSearchWireframe! { get set }
    
    func viewDidLoad()
    func viewWillAppeared(googlePlaceId: String?)
    
    func didClickedMenuStart(googleGeocode: GoogleGeocode!)
    func didClickedMenuShare()
    func didClickedPlaceInfo()
    
    func didClickedSearchBar()
    func didClickedCurrentIcon()
    func didMovedGoogleMap(_ latitude: Double, _ longitude: Double)
}

protocol DestinationSearchUsecase: class {
    weak var output: DestinationSearchInteractorOutput! { get set }
    
    func requestCoreLocation()
    func fetchCurrentLocation()
    func fetchGeocode(_ latitude: Double, _ longitude: Double)
    func fetchGeocodeFromPlaceId(googlePlaceId: String)
}

protocol DestinationSearchInteractorOutput: class {
    func validatedCoreLocation()
    func fetchedCurrenLocation(_ latitude: Double, _ longitude: Double)
    func fetchedGeocode(_ geocode: GoogleGeocode)
    func fetchedGeocodeFromPlaceId(_ geocode: GoogleGeocode)
    func fetchedError(_ error: Error)
}

protocol DestinationSearchWireframe: class {
    weak var viewController: UIViewController? { get set }

    static func assembleModule() -> UIViewController
    func presentPlaceSearch()
    func presentIlloNavigation(googleGeocode: GoogleGeocode)
}
