import Foundation
import UIKit
import GoogleMaps

enum DestinationSearchViewClickedEventType: Int {
    case current = 1, placeInfo, share, start
}

class DestinationSearchViewController: UIViewController {
    var presenter: DestinationSearchPresentation!
    
    var googlePlaceId: String?
    
    var searchBar: UISearchBar = UISearchBar()
    
    var googleMap: GMSMapView = GMSMapView()
    var centerMarker: GMSMarker = GMSMarker()
    var currentIcon: UIImageView = UIImageView()
    
    var mapInfoView: MapInfoView = MapInfoView()
    var mapMarkerView: MapMarkerView = MapMarkerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
        self.presenter.viewWillAppeared(googlePlaceId: googlePlaceId)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: UITouch in touches {
            let tag = DestinationSearchViewClickedEventType(rawValue: touch.view!.tag)
            switch tag {
            case .current?:
                self.presenter.didClickedCurrentIcon()
            case .placeInfo?:
                self.presenter.didClickedPlaceInfo()
            case .share?:
                self.presenter.didClickedMenuShare()
            case .start?:
                self.presenter.didClickedMenuStart(googleGeocode: self.mapInfoView.mapInfo?.googleGeocode)
            case .none: break
            }
        }
    }
    
}

extension DestinationSearchViewController: DestinationSearchView {
    func configureNavigationBar() {
        navigationItem.titleView = searchBar
        searchBar.showsCancelButton = false
        
        searchBar.searchBarStyle = .minimal
        searchBar.barTintColor = UIColor.white
        searchBar.delegate = self
        searchBar.placeholder = R.string.localizable.destinationSearchBarPlaceholder()
        for subView in searchBar.subviews {
            for secondSubView in subView.subviews {
                if secondSubView is UITextField {
                    let textField = secondSubView as! UITextField
                    textField.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        
        //FFEB3B
//        searchBar.tintColor = UIColor.white
    }
    func configureGoogleMapView() {
        googleMap.delegate = self
        googleMap.isMyLocationEnabled = true
        googleMap.settings.indoorPicker = true
        
        self.view.addSubview(googleMap)
        googleMap.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            //            make.top.equalTo(topLayoutGuide.snp.bottom)
            //            make.bottom.equalToSuperview()
            //            make.left.equalToSuperview()
            //            make.right.equalToSuperview()
        }
        
        currentIcon.image = R.image.icon_map_current()
        currentIcon.tag = DestinationSearchViewClickedEventType.current.rawValue
        currentIcon.isUserInteractionEnabled = true
        self.view.addSubview(currentIcon)
        currentIcon.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(topLayoutGuide.snp.bottom).offset(20)
        }
        
    }

    func configureMapInfoView() {
        self.view.addSubview(mapInfoView)
        mapInfoView.alpha = 0
        mapInfoView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-5)
            make.height.equalToSuperview().dividedBy(5)
        }
    }
    
    func configureMapMarkerView() {
        mapMarkerView.alpha = 0
        self.view.addSubview(mapMarkerView)
        mapMarkerView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-15)
        }
    }
    
    func animateGoogleMapLocation(_ latitude: Double, _ longitude: Double) {
        if self.googlePlaceId != nil {
            self.googlePlaceId = nil
        }

        self.googleMap.animate(to: GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 17))
    }
    
    func animateMapInfoView(_ geocode: GoogleGeocode) {
        self.mapInfoView.alpha > 0 ? UIView.fadeOut(component: self.mapInfoView, duration: 0.3) : UIView.fadeIn(component: self.mapInfoView, duration: 0.3)
        
        setMapInfoView(geocode)
    }
    
    private func setMapInfoView(_ geocode: GoogleGeocode) {
        self.mapInfoView.mapInfo = MapInfo(geocode, geocode.distanceFrom(lat: (self.googleMap.myLocation?.coordinate.latitude)!, lng: (self.googleMap.myLocation?.coordinate.longitude)!))
    }
    
    
    func animateMapMarkerView() {
        self.mapMarkerView.alpha > 0 ? UIView.fadeOut(component: self.mapMarkerView, duration: 0.3) : UIView.fadeIn(component: self.mapMarkerView, duration: 0.3)
    }


}

extension DestinationSearchViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        placeMarkerOnCenter(mapView.camera.target.latitude, mapView.camera.target.longitude)
//        placeMarkerOnCenter(centerMapCoordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
//        if centerMarker == marker {
//            self.presenter.didClickMarker(coordinate: centerMarker.position)
//        } else {
//            self.presenter.didClickMarker(coordinate: marker.position)
//        }
        
        return true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.presenter.didMovedGoogleMap(mapView.camera.target.latitude, mapView.camera.target.longitude)
    }
    
    func placeMarkerOnCenter(_ latitude: Double, _ longitude: Double) {
        centerMarker.icon = R.image.icon_map_pin()

        centerMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        centerMarker.map = self.googleMap
//
//        self.view.addSubview(markerView)
        
//        markerView.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.centerY.equalToSuperview().offset(-50)
//        }
        UIView.fadeOut(component: mapMarkerView, duration: 0.3)
        UIView.fadeOut(component: mapInfoView, duration: 0.3)
    }
}

extension DestinationSearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.presenter.didClickedSearchBar()
        searchBar.tintColor = UIColor.clear
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.gmsFetcher.sourceTextHasChanged(searchText)
//    }
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        self.searchBar.resignFirstResponder()
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
////        searchBar.showsCancelButton = false
//        self.searchBar.resignFirstResponder()
////        UIView.fadeOut(component: searchTableView, duration: 0.5)
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.showsCancelButton = false
//        searchBar.resignFirstResponder()
////        UIView.fadeOut(component: searchTableView, duration: 0.5)
//    }
    
}

