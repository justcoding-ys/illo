import UIKit

struct MapInfo {
    var googleGeocode: GoogleGeocode
    var distance: String
    
    init(_ googleGeocode: GoogleGeocode, _ distance: String) {
        self.googleGeocode = googleGeocode
        self.distance = distance
    }
}

class MapInfoView: UIView {
    var mapInfo: MapInfo? {
        didSet {
            self.headerAddressLabel.text = mapInfo?.googleGeocode.getAddress()
            self.headerDistanceLabel.text = mapInfo?.distance
        }
    }
    
    var containerView: UIView = UIView()
    
    var headerView: UIView = UIView()
    var headerDistanceLabel: UILabel = UILabel()
    var headerDivider: UIView = UIView()
    var headerAddressLabel: UILabel = UILabel()
    
    var contentView: UIView = UIView()
    var shareView: UIView = UIView()
    var shareImg: UIImageView = UIImageView()
    var goView: UIView = UIView()
    var goImg: UIImageView = UIImageView()
    
    var footerView: UIView = UIView()
    var placeInfoLabel: UILabel = UILabel()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViewLayer(bgColor: "FBFBFB", shadowColor: "949494")
        configureView(marginLeft: 10, marginRight: 10)
    }
    
    private func configureViewLayer(bgColor: String, shadowColor: String) {
        self.backgroundColor = UIColor(hex: bgColor)
//        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor(hex: shadowColor).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
//        self.layer.shadowRadius = 10
    }
    
    private func configureView(marginLeft: Int, marginRight: Int) {
        
        self.addSubview(containerView)
        self.containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(marginLeft)
            make.right.equalToSuperview().offset(-marginRight)
        }
        
        configureHeaderView(heightWeight: 0.3)
        configureContentView(heightWeight: 0.4)
        configureFooterView(heightWeight: 0.3)
    }
    
    private func configureHeaderView(heightWeight: Float) {
        self.containerView.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(heightWeight)
        }
        
//        R.string.localizable.mapViewInfoDistance()
//        headerDistanceLabel.text = "21m"
        headerDistanceLabel.textColor = UIColor(hex: "424242")
        headerDistanceLabel.textAlignment = .center
        headerDistanceLabel.font = UIFont.systemFont(ofSize: 16)
        headerView.addSubview(headerDistanceLabel)
        headerDistanceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.2)
            make.centerY.equalToSuperview()
        }
        
        headerDivider.backgroundColor = UIColor(hex: "9E9E9E").withAlphaComponent(0.5)
        headerView.addSubview(headerDivider)
        headerDivider.snp.makeConstraints { (make) in
            make.left.equalTo(headerDistanceLabel.snp.right).offset(8)
            make.width.equalTo(1)
            make.height.equalTo(20)
            make.centerY.equalToSuperview()
        }
        
//        headerAddressLabel.text = "김포시 태장로 845, 센트럴자이 105동 901호"
        headerAddressLabel.textColor = UIColor(hex: "9E9E9E")
        headerAddressLabel.textAlignment = .left
        headerAddressLabel.font = UIFont.systemFont(ofSize: 15)
        headerView.addSubview(headerAddressLabel)
        headerAddressLabel.snp.makeConstraints { (make) in
            make.left.equalTo(headerDivider.snp.right).offset(8)
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    func setHeaderError(message: String) {
        headerAddressLabel.text = message
    }
    
    private func configureContentView(heightWeight: Float) {
        self.containerView.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerView.snp.bottom)
            make.height.equalToSuperview().multipliedBy(heightWeight)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        contentView.addSubview(shareView)
        shareView.layer.borderWidth = 0.5
        shareView.layer.borderColor = UIColor(hex: "757575").cgColor
        shareView.layer.cornerRadius = 8
        shareView.tag = DestinationSearchViewClickedEventType.share.rawValue
        shareView.isUserInteractionEnabled = true
        shareView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.48)
            make.height.equalToSuperview().multipliedBy(1)
            make.centerY.equalToSuperview()
        }
        
        shareImg.image = R.image.icon_mapinfo_share()
        shareView.addSubview(shareImg)
        shareImg.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        contentView.addSubview(goView)
        goView.layer.borderWidth = 0.5
        goView.layer.borderColor = UIColor(hex: "757575").cgColor
        goView.layer.cornerRadius = 8
        goView.tag = DestinationSearchViewClickedEventType.start.rawValue
        goView.isUserInteractionEnabled = true
        goView.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.48)
            make.height.equalToSuperview().multipliedBy(1)
            make.centerY.equalToSuperview()
        }
        
        goImg.image = R.image.icon_mapinfo_go()
        goView.addSubview(goImg)
        goImg.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
    }
    
    private func configureFooterView(heightWeight: Float) {
        self.containerView.addSubview(footerView)
        footerView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(heightWeight)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        placeInfoLabel.text = R.string.localizable.mapViewInfoPlaceInfo()
        placeInfoLabel.textColor = UIColor(hex: "007AFF")
        placeInfoLabel.textAlignment = .right
        placeInfoLabel.font = UIFont.boldSystemFont(ofSize: 15)
        placeInfoLabel.tag = DestinationSearchViewClickedEventType.placeInfo.rawValue
        placeInfoLabel.isUserInteractionEnabled = true
        
        footerView.addSubview(placeInfoLabel)
        placeInfoLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.center.equalToSuperview()
        }
        
    }
    
}
