import UIKit

class MapMarkerView: UIView {
    var box: UIImageView = UIImageView()
    var title: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        box.image = R.image.icon_map_marker_box()
        addSubview(box)
        
        title.text = R.string.localizable.mapMarkerBoxText()
        title.textColor = UIColor(hex: "102027")
        title.font = UIFont.systemFont(ofSize: 15)
        addSubview(title)
        
        configureConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureConstraints() {
        
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        
    }

}
