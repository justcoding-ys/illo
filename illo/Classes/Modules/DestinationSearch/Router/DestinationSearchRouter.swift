import Foundation
import UIKit

class DestinationSearchRouter: DestinationSearchWireframe {
    
    weak var viewController: UIViewController?
    
    static func assembleModule() -> UIViewController {
        let view = DestinationSearchViewController()
        let presenter = DestinationSearchPresenter()
        let interactor = DestinationSearchInteractor()
        let router = DestinationSearchRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        let navigation = UINavigationController(rootViewController: view)
        navigation.isNavigationBarHidden = false
        
        return navigation
    }
    
    func presentPlaceSearch() {
        let placeSearchView = PlaceSearchRouter.assembleModule()
        viewController?.navigationController?.pushViewController(placeSearchView, animated: true)
//        viewController?.present(placeSearchView, animated: false)
    }
    
    func presentIlloNavigation(googleGeocode: GoogleGeocode) {
        let illoNaviSegmentedView = IlloNaviSegmentedRouter.assembleModule(geocode: googleGeocode)
//        let illoNaviView = IlloNavigationRouter.assembleModule(googleGeocode: googleGeocode)
        viewController?.navigationController?.pushViewController(illoNaviSegmentedView, animated: true)
    }
    
}
