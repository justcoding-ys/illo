import CoreLocation

class SplashPresenter: SplashPresentation {
    weak var view: SplashView?
    var router: SplashWireframe!
    
    func viewWillAppeared() {
        LocationManager.getInstance().configure(delegate: self).requestWhenInUseAuthorization()
//            .requestWhenInUseAuthorization()
    }
    
    func authorizedWhenInUsed(milliSeconds: Int) {
        let time = DispatchTime.now() + .milliseconds(milliSeconds)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.router.presentDestinationSearch()
        }
    }
}

extension SplashPresenter: LocationManagerDelegate {
    func didChangeAuthorization(status: LocationManagerEnum) {
        if status == .AUTHORIZED_WHEN_IN_USE {
            self.authorizedWhenInUsed(milliSeconds: 1000)
//            self.router.presentDestinationSearch()
        }
    }
    
    func didUpdateLocations(latitude: Double, longitude: Double) {}
}
