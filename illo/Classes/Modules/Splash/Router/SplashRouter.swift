import UIKit

class SplashRouter: SplashWireframe {
    
    weak var viewController: UIViewController?
    
    static func assembleModule() -> UIViewController {
        let view = SplashViewController()
        let presenter = SplashPresenter()
        let router = SplashRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.router = router
        
        router.viewController = view
        
        let navigation = UINavigationController(rootViewController: view)
        navigation.isNavigationBarHidden = true
        
        return navigation
    }
    
    func presentDestinationSearch() {
        let destinationSearchView = DestinationSearchRouter.assembleModule()
        viewController?.present(destinationSearchView, animated: false)
//        let searchPlaceViewController = SearchPlaceRouter.assembleModule()
//        viewController?.present(searchPlaceViewController, animated: false)
    }
    
}



