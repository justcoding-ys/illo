import Foundation
import UIKit

protocol SplashView: class {
    var presenter: SplashPresentation! { get set }
}

protocol SplashPresentation: class {
    weak var view: SplashView? { get set }
    var router: SplashWireframe! { get set }
    
    func viewWillAppeared() -> Void
    func authorizedWhenInUsed(milliSeconds: Int)
}

protocol SplashWireframe: class {
    weak var viewController: UIViewController? { get set }
    
    //    static func assembleModule(destination: CLLocationCoordinate2D?) -> UIViewController
    static func assembleModule() -> UIViewController
    
    func presentDestinationSearch() -> Void
}
