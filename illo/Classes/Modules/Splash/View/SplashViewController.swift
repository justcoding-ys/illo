import UIKit
import CoreLocation
import SnapKit

class SplashViewController: UIViewController {
    var presenter: SplashPresentation!
    
    var locationManager: CLLocationManager = CLLocationManager()
    var icon: UIImageView = UIImageView()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(hex: "424242")
        self.view.addSubview(self.icon)
        self.icon.image = R.image.icon_splash()
        self.icon.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter.viewWillAppeared()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        print("didapper")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
}

extension SplashViewController: SplashView {}


