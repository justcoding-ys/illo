
struct GoogleMapAPI {
    static let baseUrl = "https://maps.googleapis.com/maps/api"
}

protocol EndPoint {
    var path: String { get }
    var url: String { get }
}

enum Endpoints {
    enum GoogleApiGeocode: EndPoint {
        case fetch
        
        public var path: String {
            switch self {
            case .fetch:
                return "/geocode/json"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch:
                return "\(GoogleMapAPI.baseUrl)\(path)"
            }
        }
        
    }
}

