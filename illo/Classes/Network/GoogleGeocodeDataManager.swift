import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import RxSwift

class GoogleGeocodeDataManager {
    static let shared = GoogleGeocodeDataManager()
    
    func fetchGeocodeFromAddress(_ address: String) -> Observable<GoogleGeocode?> {
        let params: [String: Any] = ["address" : address, "sensor" : false, "key" : Constant.GoogleAPI.API_KEY]
        
        return Observable.create({ (observer: AnyObserver<GoogleGeocode?>) in
            
            let request = Alamofire.request(Endpoints.GoogleApiGeocode.fetch.url, method: .get, parameters: params).responseArray(keyPath: "results") { (response: DataResponse<[GoogleGeocode]>) in
                switch response.result {
                case .success(let geocodes):
                    geocodes.isEmpty ? observer.onNext(nil) : observer.onNext(geocodes.first!)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchGeocodeFromLatlng(_ lat: Double, _ lng: Double) -> Observable<GoogleGeocode?> {
        let params: [String: Any] = ["latlng": "\(lat),\(lng)", "key" : Constant.GoogleAPI.API_KEY]
        
        return Observable.create({ (observer: AnyObserver<GoogleGeocode?>) -> Disposable in
            let request = Alamofire.request(Endpoints.GoogleApiGeocode.fetch.url, method: .get, parameters: params).responseArray(keyPath: "results") { (response: DataResponse<[GoogleGeocode]>) in
                
                switch response.result {
                case .success(let geocodes):
                    geocodes.isEmpty ? observer.onNext(nil) : observer.onNext(geocodes.first!)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchGeocodeFromPlaceId(_ placeId: String) -> Observable<GoogleGeocode?> {
        let params: [String: Any] = ["place_id": placeId, "key" : Constant.GoogleAPI.API_KEY]
        return Observable.create({ (observer: AnyObserver<GoogleGeocode?>) -> Disposable in
            let request = Alamofire.request(Endpoints.GoogleApiGeocode.fetch.url, method: .get, parameters: params).responseArray(keyPath: "results") { (response: DataResponse<[GoogleGeocode]>) in
                
                switch response.result {
                case .success(let geocodes):
                    geocodes.isEmpty ? observer.onNext(nil) : observer.onNext(geocodes.first!)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
//    func test(_ lat: Double, _ lng: Double) {
//        let params: [String: Any] = ["latlng": "\(lat),\(lng)", "key" : Constant.GoogleAPI.API_KEY]
//        Alamofire.request(Endpoints.GoogleApiGeocode.fetch.url, method: .get, parameters: params).responseJSON { (response) in
//            print(response)
//        }
//    }
}

