import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder {
    var window: UIWindow?
}

extension AppDelegate {
    func configureNavigation() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
        
        //        navigationBarAppearace.titleTextAttributes = []
        navigationBarAppearace.barStyle = .black
        navigationBarAppearace.barTintColor = UIColor(hex: "424242")
    }
}

extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        configureNavigation()
        window = UIWindow(frame: UIScreen.main.bounds)
        RootRouter().presentSplashScreen(in: window!)
        
        return true
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//
//        configureNavigation()
//        //        print("##########################")
//        //        print("url ->\(url)")
//        //        print("url host ->\(url.host)")
//        //        print("url path ->\(url.path)")
//        //        print("lat -> \(url.valueOf("lat"))")
//        //        print("lon -> \(url.valueOf("lon"))")
//        //        print("##########################")
//
//        //TODO : ERROR 처리
//        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: (url.valueOf("lat")?.toDouble())!, longitude: (url.valueOf("lon")?.toDouble())!)
//        window = UIWindow(frame: UIScreen.main.bounds)
//        RootRouter(coordinate: coordinate).presentDirectionScreen(in: window!)
//
//        return true
//    }
}
